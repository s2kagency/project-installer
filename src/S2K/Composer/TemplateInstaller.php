<?php

namespace S2K\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class TemplateInstaller extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $prefix = substr($package->getPrettyName(), 0, 23);
        if ('s2k/template-' !== $prefix) {
            throw new \InvalidArgumentException(
                'Unable to install template, S2K templates '
                .'should always start their package name with '
                .'"s2k/template-"'
            );
        }

        return 'data/templates/'.substr($package->getPrettyName(), 23);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 's2k-template' === $packageType;
    }
}